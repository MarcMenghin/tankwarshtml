/**
created by Marc Menghin 2006-03-09


**/
var TILE_SIZE = 32;
var TILES_X = 20;
var TILES_Y = 15;
var level = 0;

/**
	builds the map set by "level"
**/
function buildMap() {
	var cur_level = LEVELS[level];
	var x;
	var y;
	
	for (y = 0; y < TILES_Y; y++)
	{
		var y_move = y * TILES_X;
		
		for (x = 0; x < TILES_X; x++)
		{
			addTile("tile", "t", cur_level[y_move + x], x, y);
		}
	}
}

/**
	loads the map set by "level". NOTE: buildMap needs to be called first
**/
function loadMap() {
	var cur_level = LEVELS[level];
	var x;
	var y;
	
	for (y = 0; y < TILES_Y; y++)
	{
		var y_move = y * TILES_X;
		
		for (x = 0; x < TILES_X; x++)
		{
			updateTile("tile", "t", cur_level[y_move + x], x, y);
		}
	}
}

function selectRandomMap() {
	var mapid = Math.floor(LEVELS.length * Math.random());
	selectMap(mapid);
}

function selectMap(lvl) {
	randomizeTileSet();
	level = lvl;
//	writeLine("Switch Level to " + String(lvl));
}

/**
	Removed all nodes from the GAME_ROOT_OBJ_ID;
**/
function clearMap() {
		clearChildNodes(getObject(GAME_ROOT_OBJ_ID), "t");
}

/**
	searches for a tile on the map and returns the first position.
**/
function findMapTile(tileID)
{
	var cur_level = LEVELS[level];
	var z;
	
	for (z = 0; z < cur_level.length; z++)
	{
		if (cur_level[z] == tileID)
			return z;
	}
	return -1;
}

/**
	Checks if the tile on this position is not free to move onto
**/
function checkForTileNotMovableOnto(pos)
{
	var cur_level = LEVELS[level];
	
	//writeLine("Tile on pos " + pos + " is " + cur_level[pos]);
	if (cur_level[pos] < 100 || cur_level[pos] >= 300)
		return true;
	
	return false;
}
