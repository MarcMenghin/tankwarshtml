
function collideTiled(player)
{
	if (!collideSpriteWithMap(player))
	{
		if (!collideTiledSpriteWithPlayer(player))
		{
			return false;
		}
	} else {
		player.speed = PLAYER_SPEED_NONE;
	}
	
	return true;
}

function collidePixel(player)
{
			if (!collideWithSpecialItems(player))
			{
				return false;
			}
	return true;
}

function collideSpriteWithMap(player)
{
	var next_tile = getTileOnPosition(player.sprite.x, player.sprite.y);
	
	//writeLine("COLLISION nextTile1: " + next_tile);
		switch(player.direction)
		{
			case PLAYER_DIR_UP:
				next_tile -= TILES_X;
				break;
			case PLAYER_DIR_LEFT:
				next_tile -= 1;
				break;
			case PLAYER_DIR_DOWN:
				next_tile += TILES_X;
				break;
			case PLAYER_DIR_RIGHT:
				next_tile += 1;
				break;
		}
	//	writeLine("COLLISION nextTile2: " + next_tile);
		
		if (next_tile < 0)
			return true;
		
		if (next_tile > 300)
			return true;
		
	return checkForTileNotMovableOnto(next_tile);
	
}

function collideTiledSpriteWithPlayer(player)
{
	var i;
	
	for (i = 0; i < ITEMS_TILED.length; i++)
	{
		var sprite = ITEMS_TILED[i].sprite;

		if (sprite.x == player.sprite.x && sprite.y == player.sprite.y)
		{
			//writeLine("Collidison Detected (TiledItem) " + player);
			//collision
			ITEMS_TILED[i].collidedWith = player;
			ITEMS_TILED[i].collision();
		}
	}
	
	return false;
}

function collideSpriteWithOtherSprites(player)
{
	return false;
}

function collideWithSpecialItems(player)
{
	var i;

	for (i = 0; i < SPECIAL_ITEMS.length; i++)
	{
		var item = SPECIAL_ITEMS[i];
		if (!item.isDead && !(player == item.player))
		{
			var itemRect = new Object();
			itemRect.x = item.sprite.x + 2;
			itemRect.y = item.sprite.y + 2;
			itemRect.w = TILE_SIZE - 4;
			itemRect.h = TILE_SIZE - 4;
//			writeLine("itemRect [" + itemRect.x + "x " + itemRect.y + "y " + itemRect.w + "w " + itemRect.h + "h]");
			
			var playerRect = new Object();
			playerRect.x = player.sprite.x;
			playerRect.y = player.sprite.y;
			playerRect.w = TILE_SIZE;
			playerRect.h = TILE_SIZE;
//			writeLine("playerRect [" + playerRect.x + "x " + playerRect.y + "y " + playerRect.w + "w " + playerRect.h + "h]");
			
			if (!(playerRect.x > (itemRect.x + itemRect.w)))
			{
				if (!(playerRect.y > (itemRect.y + itemRect.h)))
				{
					if (!((playerRect.x + playerRect.w) < itemRect.x))
					{
						if (!((playerRect.y + playerRect.h) < itemRect.y))
						{
							//writeLine("Collidison Detected (Special Items) " + player);
							//collision
							item.collidedWith = player;
							item.collision();
						}
					}
				}
			}

		}
	} //end for

	return false;
}