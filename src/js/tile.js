/**
	created by Marc Menghin 2006-03-09
**/

var TILESETTOUSE = 0;
var TILESETCOUNT = 2;
/**
Returns the URL of the tile image with this ID
**/
function getTileImgURL(tileid)
{
	var url = "img/" + String(TILE_SIZE) + "/";

	
	if (tileid >= 100 && tileid <= 199)
	{
		url = url + TILESETTOUSE + "/" + tileid + ".png";
	} else if (tileid >= 200 && tileid <= 299) {
		url = url + TILESETTOUSE + "/" + tileid + ".png";
	} else if (tileid >= 300 && tileid <= 499) {
		url = url + TILESETTOUSE + "/" + tileid + ".png";
	} else if (tileid == 620) {
		url = url + tileid + ".gif";
	} else {
		url = url + tileid + ".png";
	}

	return url;
}

function randomizeTileSet()
{
 TILESETTOUSE = Math.floor(TILESETCOUNT * Math.random());
}

/**
	returns an Object representing the Position of an tile in X and Y values.
**/
function getTilePositionXY(pos)
{
	var result = new Object();
	result.y = parseInt(pos / TILES_X);
	result.x = pos - (result.y * TILES_X);
	return result;
}

/**
	returns an object representing the rectangle of a tile.
**/
function getTileRectangleInPixel(tilePosX, tilePosY)
{
	var result = new Object();
	result.x = tilePosX * TILE_SIZE;
	result.y = tilePosY * TILE_SIZE;
	result.h = TILE_SIZE;
	result.w = TILE_SIZE;
	
	return result;
}

/**
	Returns the ID of a tile on the position. the returned string will start with "txt".
	(Uses getTileElementID2(txt, pos);
**/
function getTileElementID(txt, posX, posY) {
 return getElementID(txt, (posY * TILES_X + posX));
}

/**
	Returns True if the position is on a tile. Otherwise it returns False;
**/
function onTile(posX, posY)
{
	var x = posX % TILE_SIZE;
	var y = posY % TILE_SIZE;
//	writeLine("onTile: " + x + "x " + y + "y");
	if (x == 0 && y == 0)
	{
		return true;
	}
	
	return false;
}

function getTilePositionFromPixel(posx, posy)
{
	var x = Math.floor(posx / TILE_SIZE)
	var y = Math.floor(posy / TILE_SIZE)
	
	return ((y + TILES_X) + x);
}

/**

**/
function getTileOnPosition(x, y)
{
	return ((y / TILE_SIZE) * TILES_X + (x / TILE_SIZE));
}

/**
	Adds a Tile to the GAME_ROOT_OBJ_ID. And returns it.
**/
function addTile(className, txt, iTileID, posX, posY) {
	var root = getObject(GAME_ROOT_OBJ_ID);
	var node = window.document.createElement("div");
	root.appendChild(node);
	
	//node setup
	node.className = className + String(TILE_SIZE);
	node.id = getTileElementID(txt, posX, posY);
	
	node.style.left = String(TILE_SIZE * posX) + "px";
	node.style.top = String(TILE_SIZE * posY) + "px";
	node.style.backgroundImage = "url(" + getTileImgURL(iTileID) + ")";
	
	return node;
}

/**
	Updates a Tile to the GAME_ROOT_OBJ_ID. And returns it.
**/
function updateTile(className, txt, iTileID, posX, posY)
{
	var node = getObject(getTileElementID(txt, posX, posY));
	
	//node setup
	node.className = className + String(TILE_SIZE);
	
	node.style.backgroundImage = "url(" + getTileImgURL(iTileID) + ")";
	
	return node;
}
