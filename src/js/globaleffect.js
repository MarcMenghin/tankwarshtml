var ge_pos = 0;
var GE_MAXPOS = 128;

function loadEffect() {
	var root = getObject("globaleffect");
	root.style.backgroundImage = "url(" + getTileImgURL(400) +")";
	root.style.backgroundPosition = String(ge_pos) + "px " + String(ge_pos) + "px";
}

function moveEffect() {
	ge_pos++;
	
	if (ge_pos >= GE_MAXPOS)
		ge_pos = 0;

	var root = getObject("globaleffect");
	root.style.backgroundPosition = String(GE_MAXPOS - ge_pos) + "px " + String(ge_pos) + "px";
}
