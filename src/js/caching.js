var CACHE_ROOT = "cacheforsound";
var cachecount = 0;

function cacheImage(url)
{
	var root = getObject(CACHE_ROOT);
	
	var node = window.document.createElement("div");
	root.appendChild(node);
	
	//node setup
	node.id = getElementID("i", cachecount);
	node.className = "cacheclass";
	node.style.backgroundImage = "url(" + url + ")";
	
	cachecount++;
	
	return node;
}

function cacheSound(url)
{
	var root = getObject(CACHE_ROOT);
	
	var node = window.document.createElement("embed");
	root.appendChild(node);
	//node setup
	node.id = getElementID("i", cachecount);
	node.src = url;
	node.type = "audio/x-wav";
	node.setAttribute("autostart", "false");
	node.setAttribute("enablejavascript", "true");
	//create embeds
	// http://de.selfhtml.org/javascript/objekte/embeds.htm
	// http://www.idocs.com/tags/embeddedobjects/_EMBED.html
	// http://www.phon.ucl.ac.uk/home/mark/audio/play.htm
	
	cachecount++;
		
	return node;
}

function cacheUsedResources()
{
	writeLine("Caching resources");

	cacheImage(getTileImgURL(100));
	cacheImage(getTileImgURL(101));
	cacheImage(getTileImgURL(150));
	cacheImage(getTileImgURL(151));
	cacheImage(getTileImgURL(152));			
	cacheImage(getTileImgURL(153));			
	cacheImage(getTileImgURL(200));			
	cacheImage(getTileImgURL(320));			
	cacheImage(getTileImgURL(321));			
	cacheImage(getTileImgURL(500));			
	cacheImage(getTileImgURL(501));			
	cacheImage(getTileImgURL(502));			
	cacheImage(getTileImgURL(503));
	cacheImage(getTileImgURL(510));			
	cacheImage(getTileImgURL(511));			
	cacheImage(getTileImgURL(512));			
	cacheImage(getTileImgURL(513));
	cacheImage(getTileImgURL(520));			
	cacheImage(getTileImgURL(521));			
	cacheImage(getTileImgURL(522));			
	cacheImage(getTileImgURL(523));
	cacheImage(getTileImgURL(530));			
	cacheImage(getTileImgURL(531));			
	cacheImage(getTileImgURL(532));			
	cacheImage(getTileImgURL(533));
	cacheImage(getTileImgURL(600));
	cacheImage(getTileImgURL(620));
	
	cacheImage("img/life0.png");
	cacheImage("img/life1.png");
	cacheImage("img/life2.png");
	cacheImage("img/life3.png");
	cacheImage("img/life4.png");
	
	//SOUND_SHOOTING = cacheSound("sound/shoot.wav");
	
	writeLine("end caching imgs");
	
	//cacheSound();
	writeLine("end caching sound");
}