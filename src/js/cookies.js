

function storeCookie(name, string)
{
	var Jetzt = new Date();
	Jetzt.setYear(Jetzt.getFullYear() + 1);
	
	var cookie = name + "=" + string + "; expires=" + Jetzt.toGMTString() + "; path=/";
	//writeLine("setting cookie: " + cookie);
	document.cookie = cookie;
}

function loadCookie(name)
{
	var cookiesStr = document.cookie;
	
	//writeLine("Cookie load line: " + cookiesStr);
	
	var cookies = cookiesStr.split("; ");
	var i;
	
	for (i = 0; i < cookies.length; i++)
	{
		var valuepair = cookies[i].split("=");
		
		if (valuepair[0] == name)
		{
			return valuepair[1];
		}
	}
	
	return null;
}	
