
//Prototype item:
function ITEM_PROTOTYP() {
	this.sprite = null;
	this.collidedWith = null;
	this.img = null;
	this.player = null;
	
	//Sprite Stuff
	this.update = function() {
	};

	this.collision = function()	{
	};
	
	this.use = function () {
	};
	
	this.isDead = false;
}


function LIFE_UP() {
	this.sprite = null;
	this.collidedWith = null;
	this.img = null;
	this.delayer = 100;
	
	//Sprite Stuff
	this.update = function() {
		if (this.delayer <= 0)
				this.isDead = true;
			else
				this.delayer--;	
	};

	this.collision = function()	{
		if (this.collidedWith.life < 4)
			this.collidedWith.life++;
		
		this.collidedWith = null;
	};

	this.use = function () {
	};
	
	this.isDead = false;
}

function LIFE_SPEEDUP() {
	this.sprite = null;
	this.collidedWith = null;
	this.img = null;
	this.delayer = 200;
	
	//Sprite Stuff
	this.update = function() {
		if (this.delayer <= 0)
				this.isDead = true;
			else
				this.delayer--;	
				
		if (this.player.speed == PLAYER_SPEED_NORMAL)
			this.player.speed = PLAYER_SPEED_FAST;
	};

	this.collision = function()	{
	};

	this.use = function () {
	};
	
	this.isDead = false;
}