

var SOUND_SHOOTING = "shoot_sound";
var SOUND_EXPLOSION = "explosion_sound";

function playSound(sound) {

	var obj = getObject(sound);
	if (obj.DoPlay)
		obj.DoPlay();
	else if (obj.Play)
		obj.Play();
	else if (obj.doPlay)
		obj.doPlay();
	else if (sound.play)
		obj.play();
}
