/**

created by Marc Menghin, 2006-03-09

based on:
event loop template for JavaScript games
H. Eidenberger, 2004-03-02
**/


var datum = new Date();     // date object
var TIMER_INTERVAL = 50;  // time loop length in milliseconds
var GAME_ROOT_OBJ_ID = "gamefield"; //Basic game fiel ID
var running = false;

function initGame() {
	//Identify browser
	checkBrowser();
	
	//load last player names
	var p1 = document.playerregister.player1name;
	var p2 = document.playerregister.player2name;
	var p1name = loadCookie("human_player_1");
	var p2name = loadCookie("human_player_2");	
	
	if (p1name != null)
		p1.value = p1name;
	
	if (p2name != null)
		p2.value = p2name;

	//init highscore table	
	initHighScoreTable();
}

function pauseGame() {
	var buttontext = getObject("pausebuttontext");
	var buttonicon = getObject("pausebuttonicon");
	if (running)
	{
		setText(buttontext, "Continue");
		buttonicon.src = "img/start.png";
	} else {
		setText(buttontext, "Pause");	
		buttonicon.src = "img/pause.png";
	}
	
	running = !running; //flip running mode
}

// initialise game
function startGame() {
	resetGame();
	buildMap();
	
	//Show map
	if (!createHumanPlayers())
	{
		showBox('playerRegister');
		return;
	}
	initPlayerStats();
	putPlayersOnStartPosition();
	
	//loadEffect();

	pauseGame(); //unpause Game
	
   // start time event loop
   setTimeout(timeEvent, TIMER_INTERVAL);
}

function resetGame() {
	//Clear game stuff
	clearMap();
	clearPlayers();
	clearShoots();
	clearTiledItems();
	clearSprites();
}

// main event loop
function eventLoop(evt) {

	if (!running)
		return;

   switch(evt) {
   case "time":      // time events
	   placeItem(); //places random bonus items on map

	   //Calc AI movements
	   calcAIMovements();

			//	   moveEffect(); //moves globa effect by one step
	   updateSprites(); //Updates all Sprites
	   updatePlayerStats();
	   
	   if (checkForEndGame())
	   {
	   	//Game Ends -> player added to highscore, new level loaded all reset
		   setTimeout(handleEndGame, 200);
		   pauseGame();
	   }

		//cleanup dead objects	   
	   cleanupShoots();
	   cleanupTiledItems();
	   
      break;
   case "mouse":     // mouse events
      break;
   default:          // keyboard events
      // identify user input
      var inp;
      if (nav==2) {
         inp=event.keyCode;
      } else {
         inp=evt.which;
      } 

			handleInput(inp);
   }
}

function createHumanPlayers()
{
		var p1 = document.playerregister.player1name;
		var p2 = document.playerregister.player2name;
		
		if (p1.value == "" && p2.value == "")
		{
			alert("Pleas enter at least one Player name.");
			p1.focus();
			
			return false;
		}
		
		if (p1.value != "")
		{
			addPlayer(p1.value);
			storeCookie("human_player_1", p1.value);
		}

		if (p2.value != "")
		{
			addPlayer(p2.value);
			storeCookie("human_player_2", p2.value);			
		}
		
		return true;
}

// keyboard event handler
function keyboardEvent(evt) {
    eventLoop(evt);
}

// mouse event handler
function mouseEvent() {
   eventLoop("mouse");
}

// time event handler
function timeEvent() {
   var t1 = datum.getMilliseconds();

   eventLoop("time");

   setTimeout(timeEvent, TIMER_INTERVAL);
}

function clearGameField() {
	clearChildNodes(getObject(GAME_ROOT_OBJ_ID, ""));
}

function cacheContent() {

	//call cache stuff
	cacheUsedResources();

	//show a map ... looks better ;)
	selectRandomMap();
	buildMap();

	hideBox("cacheMSGBOX");
	showBox('playerRegister');
}

function startCaching()
{
	showBox("cacheMSGBOX");

	setTimeout(cacheContent, 500);
}

function checkForEndGame() {
	var result = true;
	var i;
	var alivePlayers = 0;
	
	//all human players dead
	for (i = 0; i < player_human_count; i++)
	{
		if (PLAYERS[i].isDead == false)
		{
			result = false;
		}
	}

	//only one player left
	if (result == false)
	{
		for (i = 0; i < MAX_PLAYERS; i++)
		{
			if (PLAYERS[i].isDead == false)
			{
				alivePlayers++;
			}
		}
		
		if (alivePlayers <= 1)
			result = true;
	}

	
	return result;
}

function handleEndGame() {
	var i;
	
	writeLine("Game Ends here");
	
	//add players to highscores
	for (i = 0; i < player_human_count; i++)
	{
		//add special score for surviving players (one at most)
		if (!PLAYERS[i].isDead)
			PLAYERS[i].highscore += SPECIAL_ITEMS_HITSCORE * 2;
		
		addPlayerToHighScore(PLAYERS[i].name, PLAYERS[i].highscore);
	}
	
	//new random map
	selectRandomMap();
	
	//restart game
	startGame();
	
	//show highscores
	updateHighScoreTable();
	showBox('topplayers');
}
// FE
